       IDENTIFICATION DIVISION. 
       PROGRAM-ID. WRITE-EMP1.
       AUTHOR. SARAWUT.
       
       ENVIRONMENT DIVISION. 
       INPUT-OUTPUT SECTION. 
       FILE-CONTROL. 
           SELECT EMP-FILE ASSIGN TO "emp1.dat"
           ORGANIZATION IS LINE SEQUENTIAL.
       
       DATA DIVISION. 
       FILE SECTION.
       FD EMP-FILE.
       01 EMP-DETAILS.
          88 END-OF-EMP-FILE              VALUE HIGH-VALUE.
       05 EMP-SSN               PIC 9(9).
          05 EMP-NAME.
             10 EMP-SURNAME     PIC X(15).
             10 EMP-FORENAME    PIC X(10).
          05 EMP-DATE-OF-BIRTH.
             10 EMP-YOB         PIC 9(4).
             10 EMP-MOB         PIC 9(2).
             10 EMP-DOB         PIC 9(2).
          05 EMP-GENDER         PIC X.

       PROCEDURE DIVISION.
       BEGIN.
           OPEN OUTPUT EMP-FILE 
           MOVE "123456789" TO EMP-SSN
           MOVE "PRAKOBKIJ" TO EMP-SURNAME
           MOVE "SARAWUT" TO EMP-FORENAME 
           MOVE "2000" TO EMP-YOB 
           MOVE "12" TO EMP-MOB
           MOVE "12" TO EMP-DOB
           MOVE "M" TO EMP-GENDER
           WRITE EMP-DETAILS 

           MOVE "234567890" TO EMP-SSN
           MOVE "SUKJAROEN" TO EMP-SURNAME
           MOVE "PUBODIN" TO EMP-FORENAME 
           MOVE "1994" TO EMP-YOB 
           MOVE "06" TO EMP-MOB
           MOVE "25" TO EMP-DOB
           MOVE "M" TO EMP-GENDER
           WRITE EMP-DETAILS 

           MOVE "832190840" TO EMP-SSN
           MOVE "LIMPRAPAIPONG" TO EMP-SURNAME
           MOVE "MANKHONG" TO EMP-FORENAME 
           MOVE "1620" TO EMP-YOB 
           MOVE "01" TO EMP-MOB
           MOVE "01" TO EMP-DOB
           MOVE "?" TO EMP-GENDER
           WRITE EMP-DETAILS 
           CLOSE EMP-FILE
           GOBACK.